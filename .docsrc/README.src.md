# Autowire
Link javascript classes with DOM nodes and make everything communicate seamlessly with one another. Extensible.

## Status: Stable, unpolished
Autowire is stable, but it is not polished. I have used it in production for quite awhile. [mtgpaper.me](https://mtgpaper.me) is a good example. It's not "open-source" but you can just look at the javascript source files anyway, as they are not minified or obfuscated currently (Apr 11, 20222).

Issues: 
- there are no proper tests, runnable from cli
- documentation could be better 
    - i have not documented the BindParam extension.
- API could be better
- its not on npm or anything

## Future Plans
See @see_file(Status.md) ... I don't have explicit plans to develop this further, but i may get around to it one day. It basically does what i need so ... 

## Install
Copy `code/Autowire.js` into your project & include it with:
```
<script src="/autowire.js"></script>
```

## Usage
For a functional example, see @see_file(test/docs/kanban/). You should be able to load @see_file(test/docs/kanban/kanban.html) into your browser 

1. Create a class, such as `class Item extends Autowire{}`. see sample class below.
2. Attach the class any of these ways 
- `new Item(node, ...args)`
- `Item.aw()`: shorthand for `Item.autowire('.Item')`
- `Item.autowire('.some-css-selector')`
- `const instance = Item.fromTemplate('.query-selector')` where the selector can point to a `<template>` & clone the inner html or a non-`<template>` and will clone the node. If there are multiple direct children of the `<template>`, then wraps all those nodes in a `<div>`

### Sample Class
This is nearly everything autowire can do. I didn't include stuff about how `onReady()` works or a couple other primarily internal things.
```js
@file(test/docs/minimal/item.js)
```

### PHP Integration
Get the file path to Autowire.js or another js file in this repo & add it to the page.
```php
<?php
$autowire_path = \Tlf\Js\Autowire::filePath();
$modal_path = \Tlf\Js\Autowire::filePath('Modal.js');
$bind_param_path = \Tlf\Js\Autowire::filePath('BindParam.js');
$dropdown_path = \Tlf\Js\Autowire::filePath('addon/Dropdown.js');
```
