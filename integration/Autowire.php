<?php

namespace Tlf\Js;

/**
 * Get absolute path to js files in this repo
 *
 * @todo add composer.json file
 */
class Autowire {
    
    /**
     * Get the path to the file or null
     * @param $name name of the js file to get, or 'Autowire.js' by default
     */
    static public function filePath($name="Autowire.js"){
        $file = dirname(__DIR__).'/code/'.$name;
        if (file_exists($file))return $file;

        return null;
    }
}
