
class Dropdown extends Autowire {
    get output(){
        return document.getElementById('test');
    }

    async results(){
        const json = await this.fetchText(this.n.getAttribute('x-target'), {"query":this.n.value});
        const items = JSON.parse(json);
        return items;
    }

    async onkeyup(){
        const items = await this.results();
        console.log(items);
        const txt = 'ok';
        this.output.innerHTML = txt;

        const dropdown = document.createElement('div');
        for (const item_text of items){
            const item = document.createElement('div');
            item.innerText = item_text;
            dropdown.insertBefore(item,null);
            new DropdownItem(item, this);
        }

        this.output.innerHTML = '';
        this.output.insertBefore(dropdown,null);
    }
}

class DropdownItem extends Autowire {

    onCreate(Dropdown){
        this.input = Dropdown.n;
    }
    onclick(){
        this.input.value = this.innerHTML;
    }
}
