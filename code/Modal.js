
Aw.Modal = class extends Autowire {
    static exampleUsage(){

        const modal = Aw.Modal.fromTemplate('template.Modal.Column');
        // or
        // const modal = Aw.Modal.fromUrl('/whatver', optionalData);

        modal.onComplete = function(formData){
            const col = this.fromTemplate('template.Column');
            col.q('h2').innerText = formData.name;
            this.node.appendChild(col);
        }

        modal.show();
    }

    onAttach(){
        console.log(this.node);
    }

    show(){

    }


    static fromTemplate(selector){
        const root = this.prototype.fromTemplate(selector);
        const modal = new this.prototype.constructor(root);

        return modal;
    }
}
