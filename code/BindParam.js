/**
 * `{{param}}` bindings. Early prototype. It is used in the vue-compare test.
 *
 * call `YourClass.use('Bind')`, then use a property like `{{this.propName}}`
 * @todo replace `<span data-awbindparam="paramName">` approach with something better.
 *    - Simply re-rendering a string-replacement doesn't work because it breaks click listeners & things
 *
 */
class Bind {

    static onExtAttach(){
        const template = this.n.cloneNode(true);
        const regParams = /\{\{this\.([a-zA-Z_]+)\}\}/g;
        const matchList = [...template.innerHTML.matchAll(regParams)];
        let bindHtml = template.outerHTML;
        for (const match of matchList){
            const param = match[1];
            const paramHtml = '<span data-awbindparam="'+param+'"></span>';
            bindHtml = bindHtml.replace('{{this.'+param+'}}', paramHtml);

            Object.defineProperty(this, param,
                {
                    get: function(param){
                        return this.q('span[data-awbindparam="'+param+'"]').innerText;
                    }.bind(this, param),
                    set: function(param, value){
                        this.q('span[data-awbindparam="'+param+'"]').innerText = value;
                    }.bind(this, param)
                }
            );
            Object.defineProperty(this, param+'Node',
                {
                    get: function(param){
                        return this.q('span[data-awbindparam="'+param+'"]');
                    }.bind(this, param)
                }
            );
        }
        const placeholder = document.createElement('span');
        placeholder.innerHTML = bindHtml;
        const newNode = placeholder.children[0];
        newNode.parentNode.removeChild(newNode);
        if (this.node.parentNode!=null){
            this.node.parentNode.replaceChild(newNode,this.n);
        }
        this.node = newNode;
        this.n = newNode;
    }
}

Autowire.expose('Bind', Bind);
