<?php


require(__DIR__.'/vendor/autoload.php');

new \Phad();

$dir = __DIR__.'/';

$lia = new \Lia\Simple();
$lia->debug = !$lia->is_production(); 

$lia->root_dir = __DIR__;


$lia->deliver_files("$dir/static/");
// $lia->load_apps("$dir/apps/Main", "$dir/apps/Example");
$lia->load_apps("$dir/apps/Main", "$dir/apps/Autowire");
$lia->setup();

$lia->respond();
