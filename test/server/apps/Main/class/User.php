<?php

namespace Lia\Example;

class User extends \Tlf\User {

    public $name = 'User';

    public function isGuest(){
        if ($this->is_registered())return false;
        return true;
    }
}
