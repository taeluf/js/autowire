<?php

$search = $_POST['query'];

$data = [
    'apple',
    'anonymous',
    'androgynous',
    'analgesic',
    'bat',
    'bar',
    'bill',
    'cat',
    'carry',
    'cold',
    'dog',
    'dig',
    'dug',
    'elephant',
    'electrolysis',
    'entrophant',
    'encepholytis',
    'fallible',
    'finite',
    'fun',
    'ghost',
    'ghoul',
    'given',
    'gallatious',
    'gastranonymous',
    'galloping',
    'gastroenterologist',
    'gab'
];

$len = strlen($search);
$out = array_filter($data,
    function($item) use ($search, $len){
        if (substr($item,0,$len)==$search){
            return true;
        }
        return false;
    }
);

echo json_encode(array_values($out));
exit;
