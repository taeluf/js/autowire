
class Item extends Autowire {

    /** generally, you shouldn't implement constructor */
    constructor(node, ...args){
        super(node,...args);
    }
    /** called from constructor after this.node is set 
     * @parma args are same as passed to new `Item(node, ...args);` 
     */
    onCreate(...args){
        this.node.innerHtml = 'whatever';
    }
    /** called after:
     * - event listeners are setup 
     * - this object is added to the object map 
     * - extensions are initialized
     */
    onAttach(...args){
        this.child_item = new \ChildItem(document.querySelector('.child-item'));
        // change `this` to this class for any `on` events declared in html, such as `<button onclick="this.something()">` would now reference Item instead of the node
        this.bindTo(this.node);
        //this.bindToAll(node_list) will bind to a node list
    }
    /**
     * called after all autowire objects are setup
     */
    onReady(){
        // get all child objects of this.node. this.ga() is a shorthand
        const children = this.getAnyList('ChildItem');
        // get one child object of this.node. this.g() is a shorthand
        const child = this.getAny('Child');

        const parentNode = document.querySelector('something.whatever');j
        // get all items of class name that are children of parentNode. parentNode may be null
        const object_list = Autowire.getObjectsFromName('ClassName', parentNode);
        // get the Autowire instance attached to the given node
        const object = Autowire.getObjectFromNode(parentNode);
    }

    /**
     * called when this.node is clicked. Even listeners are added for any method starting with `on`
     *
     * async is not required, but makes it easy to `await this.fetchJson()`
     */
    async onclick(event){
        // this.fj() for shorthand
        const new_data = await this.fetchJson('/data/',{"key":"value"}, "POST");
        // this.ft() for shorthand
        const new_text = await this.fetchText('/text/', {"key":"value"}, "GET");
        const response_promise = await this.fetch(/*same params*/);
        const other_text = response_promise.text(); //or .json() for json
    }

}

// apply an extension
Item.aw();


class Alerter {
    // called before onAttach() & after onCreate()
    static onExtAttach(){
        // `this` refers to the Autowire object being attached to
        const node = this.node;
        this.alert = function(msg){alert("ALERTED!!! "+msg);};
        // basically just modify the node & the object however you want here ...
        // such as Object.defineProperty() for ... idk, whatever
    }
}

Autowire.expose('Alerter', Alerter);

// turn all Item instances into Alerters
Item.use(Alerter);
