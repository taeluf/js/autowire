
class KanbanBoard extends Autowire {
    get boardName(){return this.q('h1').innerText;}   
    get colInput(){return this.q('input');}
    get cardInput(){return this.q('textarea');}

    onAttach(){
        // so the buttons with [onclick] will directly call this object with `this.newColumn()` & `this.newCard()`
        this.bindToAll(this.qa('[onclick]'));
        //@todo this.wire('div', 'KanbanColumn'); // a shorthand for the following loop
        for (const n of this.qa('div')){
            new KanbanColumn(n, this);
        }
    }

    /** hooked into by a button */
    newCard(){
        alert("Click on a column to select it.");
        this.awaitingColumn = true;
    }
    /** hooked into by a button */
    newColumn(){
        const node = this.fromTemplate('template.Column');
        const column = new KanbanColumn(node, this, this.colInput.value);
        this.colInput.value = '';
        this.node.appendChild(node);
    }

    /** called column's onclick */
    insertCardInto(col){
        if (!this.awaitingColumn)return;
        this.awaitingColumn = false;
        const node = this.fromTemplate('template.Card');
        node.innerText = this.cardInput.value;
        this.cardInput.value = '';
        col.q('ul').appendChild(node);
    }
}

KanbanBoard.aw();

/**
 * Minimal utility class for columns.
 */
class KanbanColumn extends Autowire {
    get h2(){return this.q('h2');}

    onAttach(Board, name=null){
        this.board = Board;
        if (name!=null) this.h2.innerText = name;
    }

    onclick(){
        this.board.insertCardInto(this);
    }   
}
