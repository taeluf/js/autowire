# Addons
There may be other docs somewhere covering other addons. I've made this doc just for Dropdown.js, but intend to use this doc for future addons I write too.

## Dropdown.js
What: Sets up a text input to query for a list of options, display them as a dropdown list, and update the text input when an option is clicked on
Status: Jank, but technically functional. There is no styling or polish.
Howto:
- include `Autowire.js` & `Dropdown.js` on the page
- create a `text` input, add attribute `class="Dropdown"`, add attribute `x-target="/some-url/"`
- call `Dropdown.aw()`
- create a node with `id="output"` where results will be displayed
- POST `/some-url/` must return a json array of options to display. It must not have any keys, even numeric ones. use `array_values()` before `json_encode()`

