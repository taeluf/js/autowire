/**
 * This seeks to provide `{{param}}` bindings. It's an early prototype. It is NOT robust. It is used in the vue-compare test.
 */
class Bind {
    fill(param, value){
        const targets = this.qa('[data-bind="'+param+'"]');
        for (const n of targets){
            n.innerHTML = value;
        }

        const inputs = this.qa('input[value="{{'+param+'}}"]');
        for (const i of inputs){
            i.value = value;
        }
    }

    static onExtAttach(){
        const template = this.n.cloneNode(true);
        const inputs = template.querySelectorAll('input');
        console.log(inputs);
        for (const i of inputs){
            console.log('wildcard'+ i.value);
            if (i.value.substr(0,2)=='{{' && i.value.substr(-2) == '}}'){ // == '{{*}}'){
                const param = i.value.substr(2, i.value.length-4);
                i.addEventListener('keyup', Bind.inputKeyUp.bind(i,this,param));
                console.log('param:'+param);
                const others = template.querySelectorAll('*:not(input)');
                for (const n of others){
                    n.outerHTML = n.outerHTML.replaceAll("{{"+param+"}}", '<span data-bind="'+param+'"></span>');
                }
            }
        }


        this.bindTemplate = template;
        this.n.parentNode.replaceChild(template,this.n);
        this.n = template;
    }

    static inputKeyUp(item, param, event){
        item.fill(param, this.value);
    }
}

Autowire.expose('Autowire.Bind', Bind);