/**
 * 
 * This is just code I'm holding onto in case I decide to use it in the future. Most of it was used in previous versions of Autowire.
 */

//-----------------------
// a crummy object mapper (isn't this a feature already available in JS? Idunno)
// ------------------
Autowire.Tools.objectId = function(object){
    this.objectMap = this.objectMap || new WeakMap();
    if (this.objectMap.has(object)){
        return this.objectMap.get(object);
    }
    
    this.idMap = this.idMap || {};
    this.counter = this.counter || 0;
    const id = object.constructor.name+":"+(new Date()).getMilliseconds() + "-" + Object.keys(this.objectMap).length + "-" + this.counter++ + "-" + Math.random();
    this.idMap[id] = object;

    this.objectMap.set(object,id);
    
    return id;
}.bind(Autowire.Tools);

Autowire.Tools.objectFor = function(id){
    return this.idMap[id];
}.bind(Autowire.Tools);
//------------------------------
// end crummy object mapper
//------------------------------

//-----------------------------
// get all methods on an object
//------------------------------
function getMethods(object){
    const properties = new Set();
    let currentObj = object;
    do {
        Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
    } while ((currentObj = Object.getPrototypeOf(currentObj)))
    return [...properties.keys()].filter(item => typeof object[item] === 'function')

    // These following lines (with fixes) would remove methods that are defined on the autowire prototype,
        // so only methods on subclasses of autowire are returned


    const methodNames = Autowire.Tools.getObjectMethodNames(this);
    const rootMethods = Autowire.Tools.getObjectMethodNames(Autowire.prototype);
    const childMethods = methodNames.filter(method => rootMethods.indexOf(method)<0);
    return childMethods;
}
//-----------------------------
// end get all methods on an object
//------------------------------


//-----------------------------
// Some Dom Tools
//------------------------------
class DomTools {

    /**
     * toggle an attribute on a node 
     */
    static cycle(node,attribute,value=null){
        if (value==null
            &&node.hasAttribute(attribute)){
                node.removeAttribute(attribute);
        } else if(value==null){
            node.setAttribute(attribute,'');
        } else {
            // const index = value.indexOf(node[attribute]);
            // index++;
            // index = index%value.length;
            node[attribute] = value[ (1+value.indexOf(node[attribute]))%value.length ];
        }
    }

    /**
     * Check if a node is the given tagname
     */
    static is(tagName,node){
        if (node.tagName.toLowerCase()==tagName.toLowerCase())return true;
        return false;
    }
}

//-----------------------------
// end dom tools
//------------------------------