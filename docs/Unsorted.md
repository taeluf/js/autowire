<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Unsorted Documentation  
  
## Terms  
- Wired Object: An Autowire subclass which has been instantiated and attached to a node.   
  
## Getting Started Checklist  
When you set up a new Autowire class, you need to ensure:  
- `class ClassName {}` is declared  
- `ClassName.aw()` is called (usually immediately after the class definition)  
- `<node class="ClassName">` is declared  
Make sure:  
- `ClassName` matches in all three places  
- You've included `Autowire.js` and your own `.js` file in the html page.  
- There are no javascript errors in the browser console  
- Your node is being attached successfully:  
    - In `onAttach(){}`, write `console.log('I found the node!')` or something.  
  
## Minor convenience things  
- `myObj.innerHtml` is a shorthand for `myObj.node.innerHtml`  
  
## Extensions  
- Your extension must override `onExtAttach()`.   
    - This is called after `onCreate(...args)` and receives `...args`  
    - `this` refers to the instantiated autowire subclass this extension is attached to  
    - `this.node` is available.   
    - You can do literally whatever you want here. Though, beware of things that may be overridden during the `attach` step.  
- `Autowire.expose(extensionKey, uninstantiatedClassObject)`: Make an extension available to `use`  
    - This is used by an extension developer to make their extension available.   
  
## Wiring Objects  
These help you attach an Autowire class to a node  
- `new MyModal(nodeObject, ...args)`: This is often the way to go if you have nested nodes to wire up, since you can pass `this` to the nested node & it will have a reference to its root node.  
- `MyModal.aw('Accept Terms?')` will autowire `MyModal` class to any nodes matching `.MyModal` css selector and passes `Accept Terms?` to `onCreate()` and `onAttach()` and `onReady()`.  
- `MySubClass.autowire(querySelector=null, onPageLoad=true, ...args)`  
    - if `querySelector` is not provided, then default of `.MySubClass` is used.  
    - pass `onPageLoad` as `false`, and the wiring will happen immediately instead of waiting for page load to complete.   
    - if `onPageLoad` is `true`, AND the page's loading is already complte, then wiring will be done immediately.  
  
## Core static functions  
- `MySubClass.use(extensionKey)`  
    - Enable the named extension on the given class.  
- `getObjectsFromName(className=null,parentObject=null)`.  
    - Get all objects with the given class name, optionally only the ones who's nodes are children of the parent object's node  
    - if `className` is not provided, the called class's name is used.  
        - Ex: `MyModal.getObjectsFromName()` is the same as `Autowire.getObjectsFromName('MyModal')`  
- `getObjectFromNode(node)`: Gets the object wired to the node or null  
- `onPageLoad(func, thisArg, ...args)`: Will execute the given function when the page loads, binding the `thisArg` & list of `...args`. Executes immediately if the page is already loaded.  
  
## Core properties to use  
- `this.node`. The node we're attached to  
    - Shorthand: `this.n`  
- `this.name`. The class name  
  
## Core methods to override  
- `onCreate`  
    - Called before extensions are applied & listeners are setup. this.name & this.node are available  
    - Receives any args passed to the class constructor  
- `onAttach`  
    - Called after the node is completely ready. Click listeners are setup. Other nodes may not be setup yet.  
- `onReady`  
    - Called when there are no pending calls to autowire.  
    - This does not run if you create an autowire object directly (`new Autowire(node)`).  
    - Not sure if it gets called after initial page load... I think so?  
  
## Core methods to call  
- `q('.selector')`.   
    - Shorthand for `this.node.querySelector('.selector')` (only searches children of this node)  
- `qa('.selector')`.   
    - Shorthand for `this.node.querySelectorAll('.selector')`  
- `getAny('ClassName')`:   
    - Shorthand: `g('ClassName')`   
    - Gets a Wired Object with the given classname  
    - Longhand: `Autowire.getObjectsFromName('ClassName')`  
- `getAnyList('ClassName')`  
    - Shorthand: `ga('ClassName')`  
    - Gets a list of Wired Objects with the given classname  
- `fromTemplate('.selector')`  
- `fetch('/url',paramsObject, method="POST")`: Returns a response promise. You need to `await response.text()` or `.json()`  
    - Shorthands: `fetchText()`, `fetchJson()`, `ft()` & `fj()` all with the same paramater list. Simply do: `text = await ft('/whatever/');`  
- `bindTo(node)`  
    - Bind `this` to each attribute-listener on node.   
    - An `onclick` declared in html will have the called class as `this`, rather than the node.  
- `bindToAll(nodeList)`  
    - same as `bindTo` except it takes a NodeList.  
  
  
