# Development Status

## June 2, 2023
Add php integration documentation to the readme. added code scrawl dev dependency & changed scrawl configration to NOT scan `test/` dir.

## Nov 13, 2022
- Updates: Create `Dropdown.js`, setup `test/server/` for easier testing using Liaison php webserver
- TODO: style & polish Dropdown, update `autowire.fetch()` to work with GET requests. `GET` requests cannot have a body.
- Info: `test/server` is a liaison server. `test/server/apps/autowire/` contains specific feature testing. in `public/index.php` is the dropdown test. It uses `public/@POST.getlist.php` to populate a dropdown list.

## Latest
- finish kanban test
- add html binding

## v0.1-candidate Next
- Use Code Scrawl to output the Kanban test into documentation
- Organize the unsorted documentation
- Clean up the README
- Move to `v0.1` branch
- Move html binding notes into its own file
- release

## Next
- Release `v0.1`
- Prepare a `v0.2` release with breaking changes, but no major overhaul to the codebase.

## v0.2 Next
- Automate kanban test with Jasmine
- Generate documentation with Code Scrawl via the Lexer's Grammar (rather than custom written docs as I currently have)
- Add a `composer.json` for php installation (probably, since I use php dev tools for it)
- Improve javadocs. Add `@return`s and just better explanations
- Write a `flow` documentation that explains the basic process for how everything works

### code changes
- Review names of functions & consider changing them / cleaning them up. 
- Rename `use` and `expose` to something more intuitive
- Add additional `getAny('ClassName')` methods:
    - Add `parent('ClassName')` get an object by walking UP the DOM tree and checking each node for an attached object. 
    - `child('ClassName')` call `getAny()` with `this` as a reference node
    - Add `sibling('ClassName', 'ParentClassName')` which will first call `parent('ParentClassName')` then call `child('ClassName')` on the parent.  
- Review functions & make sure all that SHOULD be static actually are & visa versa
- Reorganize the class, so the separate categories of functionality are separated 
- Add a convenience method to attach a class to each of a list of querySelected nodes:
    ```
        for (const n of this.qa('div')){
            new KanbanColumn(n, this);
        }
    ```


## v0.3 Next
- (maybe) Make a new `use()` function that works like php's trait's feature.

## Html Binding
Make it so `ondblclick=this.askToClose()` calls `instanceOfModal.askToClose()`
```html
<script>
    class Modal {
        
        askToClose(){
            if (confirm("Close Modal?")){
                this.close();
            }
        }
        close(){
            this.n.style.display = 'none';
        }
    }
</script>
<div class="Modal" ondblclick="this.askToClose">
</div>
```
Internals:
1. Loop over all attributes of that `<div>`. 
2. For each attribute that starts with `on`, check for `node[theonmethod]` 
3. if its available & is a function, then `node[theonmethod].bind(theModalInstance)`
4. When the natural `dblclick` event is called on the `<div>` it will call `theModalInstance.askToClose`


## Modal
I definitely want to add a Modal class that is a subclass of autowire. However, I don't want to do it now.
This is a sample template of a modal, but this is incomplete.
```html
<template class="Modal Column">
    <form>
        <label>
            Column Name<br>
            <input name="name" type="text" placeholder="Column Name" />
        </label>
        <button onclick="this.cancel">Cancel</button>
        <button onclick="this.complete">Submit</button>
    </form>
</template>
```

### The Challenge
There are two major approaches to the modal: Provide bells & whistles (css, buttons, a11c), or just provide a js backend with some api of stuff to hook into (`onclick="this.close"` or `this.complete`, etc).
I want to provide a bells & whistles approach so that its actually easy to use. But I also want to provide the minimalist js backend (with no frontend) and not make any decisions for the user.
But, since its just css, javascript, and html ... maybe I can figure it out.

Also, how the heck to web components work? Can I make a node `<Aw-Modal>` & then use that in my template? So my modal template would look like:
```
<Aw-Modal>
    <form>
        <h2>Whatever</h2>
        <Aw-Input name="whatever" type="whatever"></Aw-Input>
    </form>
</Aw-Modal>
```
This would all expand into something like:
```
<template class="Modal Column">
    <form>
        <label>
            Column Name<br>
            <input name="name" type="text" placeholder="Column Name" />
        </label>
        <button onclick="this.cancel">Cancel</button>
        <button onclick="this.complete">Submit</button>
    </form>
</template>
```

I think the components thing is worth looking into, but only worth implementing if there is browser support. If I have to manually do a bunch of attribute copying or something ... I don't think its worth it.
